const express = require("express");
const app = express();
var body_parser = require('body-parser').json();
const request = require('request');
var fecha = new Date();

//Index 
app.get('/', function (req, res) {
    res.send('Saludos desde Servidor Cliente');

});

//Genera orden
app.get('/orden', function(req,res){
    var orden = Math.floor(Math.random() * (100-1)+1)
    var descripcion = "Se creo una orden con identificador: "+orden
    request.post('http://localhost:3004/log',{
        json:{
            'descripcion':descripcion
        }
    })
    request.post('http://localhost:3000/postorden',{
        json:{
            'idorden':orden
        }
    })
    var fecha = new Date();
    res.json({'idorden':orden , 'descripcion':descripcion, 'fecha': fecha })    
})

// Puerto 3001
app.listen(3001, () => {
 console.log("El servidor Cliente está inicializado en el puerto 3001");
});