const express = require("express");
const app = express();
var body_parser = require('body-parser').json();
const request = require('request');

app.get('/', function (req, res) {
    res.send('Saludos desde Servidor Repartidor');

});

app.post('/orden',body_parser, function(req,res){
    var orden = req.body.idorden
    var repa = req.body.idrepartidor
    var descripcion = "Se recibio orden: "+orden+" para repartidor:"+ repa
    request.post('http://localhost:3004/log',{
        json:{
            'descripcion':descripcion
        }
    })
    res.send("OK")
});

app.get('/estado/:entrega',body_parser, function(req,res){
    var orden = req.params.entrega
    var descripcion = "Repartidor recibe una consulta de la entrega de la orden: "+orden
    request.post('http://localhost:3004/log',{
        json:{
            'descripcion':descripcion
        }
    })
    var aLetras = new Array('a', 'b', 'c');
    var cLetra = aLetras[Math.floor(Math.random()*aLetras.length)]; 

    if(cLetra == 'a'){
        descripcion = 'Entrega: '+orden+' Estado: '+' Recogiendo de restaurante'
    }else if (cLetra == 'b'){
        descripcion = 'Entrega: '+orden+' Estado: '+' Entregada'
    }else {
        descripcion = 'Entrega: '+orden+' Estado: '+' En camino'
    }
    request.post('http://localhost:3004/log',{
        json:{
            'descripcion':descripcion
        }
    })
    res.json({'state': cLetra , 'descripcion':descripcion})
});

app.listen(3002, () => {
 console.log("El servidor Repartidor está inicializado en el puerto 3002");
});