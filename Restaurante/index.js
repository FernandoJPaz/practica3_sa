const express = require("express");
const app = express();
var body_parser = require('body-parser').json();
const request = require('request');

//Index
app.get('/', function (req, res) {
    res.send('Saludos desde Servidor Restaurante');

});

//Agrego orden al restaurante 
app.post('/postorden',body_parser, function(req,res){
    var order = req.body.idorden
    var descripcion = "Restaurante recibe una orden: "+order
    request.post('http://localhost:3004/log',{
        json:{
            'descripcion':descripcion
        }
    })
    descripcion = "Restaurante prepara orden: "+order
    request.post('http://localhost:3004/log',{
        json:{
            'descripcion':descripcion
        }
    })
    res.send("OK")
});

//Estado de la orden
app.get('/estado/:orden',body_parser, function(req,res){
    var orden = req.params.orden
    var descripcion = "Restaurante recibe una consulta de orden: "+orden +" para saber el estado de la misma"
    request.post('http://localhost:3004/log',{
        json:{
            'descripcion':descripcion
        }
    })
    var aLetras = new Array('a', 'b', 'c');
    var cLetra = aLetras[Math.floor(Math.random()*aLetras.length)]; 

    if(cLetra == 'a'){
        descripcion = 'Orden: '+orden+' Estado: '+' Preparandose'
    }else if (cLetra == 'b'){
        descripcion = 'Orden: '+orden+' Estado: '+' Enviandose'
    }else {
        descripcion = 'Orden: '+orden+' Estado: '+' No Recibida y Cancelada'
    }
    request.post('http://localhost:3004/log',{
        json:{
            'descripcion':descripcion
        }
    })
    res.json({'state': cLetra , 'descripcion':descripcion})
});

//Asignacion de orden y cliente para x Repartidor 
app.post('/Entrega',body_parser,function(req,res){
    var orden = req.body.idorden
    var descripcion = "La orden: "+orden+" esta lista para enviar"

    request.post('http://localhost:3004/log',{
        json:{
            'descripcion':descripcion
        }
    })

    var Repartidor = Math.floor(Math.random() * (100-1)+1)
    descripcion = "Repartidor:"+Repartidor+" repartira orden: "+orden
    
    request.post('http://localhost:3004/log',{
        json:{
            'descripcion':descripcion
        }
    })
    
    request.post('http://localhost:3002/orden',{
        json:{
            'idorden': orden,
            'idrepartidor': Repartidor
        }
    })
    
    res.json({'idorden': orden,'idrepartidor':Repartidor})
    
});



//Servidor run en puerto 3000
app.listen(3000, () => {
 console.log("El servidor Restaurante está inicializado en el puerto 3000");
});

